# Quick Invoice

Simple tool to speed up boring stuff about invoices (writing this tool was not boring at all :)). 

## Getting Started

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

Open up the project in IDE build it and take care of dependencies with maven.

```
mvn install 
```

Once you get jar file in target folder you can run the program with the command below.

```
java -jar target/invoice-app-2-1.0-SNAPSHOT.jar
``` 

## Deployment

todo (consider generating actual executable for different OS).

## Changelog

todo

## Documentation

At the moment there is no documentation for this project.

## Built With

* [Java](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository. 

## Authors

Most of initial content for this project was provided by Matic Zagmajster. For more information please see ```AUTHORS``` file.

## License

This project is licensed under the MIT License - see the ```LICENSE.md``` file for details.

## Acknowledgments

* [CalliCoder](https://www.callicoder.com/about/) deserves the shout out for good tutorial on QR codes  

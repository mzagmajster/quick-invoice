SELECT i.invoice_date AS invoicePosted, -- datum knjizbe
  i.number AS invoiceNumber,
  i.invoice_date AS invoiceCreated,
  ic.column_1 AS workTo,
  i.summary AS summary,
  c.unique_name AS clientName,
  ia.total AS total,
  -- Additional fields.
  i.id AS invoiceId,
  CASE
    WHEN i.invoice_status_id = 1 THEN 'osnutek'
    WHEN i.invoice_status_id = 2 THEN 'poslan'
    WHEN i.invoice_status_id = 3 THEN 'plačan'
    WHEN i.invoice_status_id = 4 THEN 'preklican'
  END AS invoiceStatus,
  ic.column_2 AS workFrom
FROM invoices i
  INNER JOIN invoices_custom ic ON (i.id = ic.invoice_id)
  INNER JOIN clients c ON (i.client_id = c.id)
  INNER JOIN invoice_amounts ia ON (i.id = ia.invoice_id)
WHERE i.deleted_at IS NULL AND YEAR(i.invoice_date) = ? ORDER BY i.invoice_date;
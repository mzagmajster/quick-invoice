SELECT 	i.id AS IdRacuna,
    c.id AS IdStranke,
    c.unique_name AS placnikIme,
    c.name AS placnikPolnoIme,
	c.address AS placnikUlica,
    CONCAT(c.zip, ' ', c.city) AS placnikKraj,
	c.email AS strankaEposta,
	i.number AS stevilkaRacuna,
	i.url_key AS javnaUrlPot,
	ia.total AS znesek,
	ic.column_1 AS obdobjeDo,
	ic.column_2 AS obdobjeOd,
	DATE_FORMAT(i.due_at, '%d.%m.%Y') AS rokPlacila,
	REPLACE(cpc.column_3, ' ', '') AS prejemnikIban,
	cpc.column_4 AS prejemnikIme,
	cp.address AS prejemnikUlica,
	CONCAT(cp.zip, ' ', cp.city) AS prejemnikKraj
FROM invoices i
INNER JOIN invoice_amounts ia
    ON (i.id = ia.invoice_id)
INNER JOIN clients c
	ON (i.client_id = c.id)
INNER JOIN invoices_custom ic
	ON (i.id = ic.invoice_id)
INNER JOIN company_profiles cp
    ON (i.company_profile_id = cp.id)
INNER JOIN company_profiles_custom cpc
    ON (i.company_profile_id = cpc.company_profile_id)
WHERE i.id = ?;
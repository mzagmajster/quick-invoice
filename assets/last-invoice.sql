SELECT invoice_date AS invoiceCreated,
    CASE
        WHEN invoice_status_id = 1 THEN 'osnutek'
        WHEN invoice_status_id = 2 THEN 'poslan'
        WHEN invoice_status_id = 3 THEN 'plačan'
        WHEN invoice_status_id = 4 THEN 'preklican'
      END AS invoiceStatus
FROM invoices
WHERE client_id = ? AND deleted_at IS NULL
ORDER BY created_at DESC;
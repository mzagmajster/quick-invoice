SELECT id AS clientId,
  name AS name,
  unique_name AS alias
FROM clients
WHERE active = 1 AND deleted_at IS NULL
ORDER BY alias;
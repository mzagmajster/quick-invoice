package com.mzagmajster.invoice;

public class Client {
    private Integer clientId;

    private String alias;

    private String name;

    public Client (Integer clientId, String alias, String name) {
        this.clientId = clientId;
        this.alias = alias;
        this.name = name;
    }

    public Integer getClientId() {
        return clientId;
    }

    public String getAlias() {
        return alias;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return alias;
    }
}

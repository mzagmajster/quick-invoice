package com.mzagmajster.invoice.gui;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.awt.*;
import java.time.Year;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.sql.SQLException;
import java.net.URL;
import java.net.MalformedURLException;
import com.google.zxing.WriterException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.mzagmajster.invoice.QrGenerator;
import com.mzagmajster.invoice.DataCollector;
import com.mzagmajster.invoice.Report;
import com.mzagmajster.invoice.SignatureGenerator;
import com.mzagmajster.invoice.Client;
import com.mzagmajster.invoice.Invoice;

public class MainForm {
    private JPanel quickInvoicePanel;
    private JComboBox clientList;
    private JTextField clientNameField;
    private JTextField clientLastInvoiceOnField;
    private JTextField clientLastInvoiceStatusField;
    private JButton newReportButton;
    private JTextField invoiceIdField;
    private JButton generateButton;
    private JTextField emailSubjectField;
    private JLabel clientLabel;
    private JLabel clientNameLabel;
    private JLabel clientLastInvoiceOnLabel;
    private JLabel clientLastInvoiceStatusLabel;
    private JPanel reportPanel;
    private JPanel upnCodePanel;
    private JPanel emailMessagePanel;
    private JLabel emailAddressLabel;
    private JLabel emailSubjectLabel;
    private JLabel emailValueLabel;
    private JLabel statusLabel;
    private JLabel statusValueLabel;
    private JToolBar statusToolBar;
    private JLabel upnCodeLabel;
    private JButton signInvoiceButton;
    private JButton downloadPDFButton;
    private JTextField localInvoiceFolderField;
    private JLabel localInvoiceFolderLabel;
    private JPanel otherComponentsPanel;
    private JButton generateEndYearReportButton;

    private JsonObject appConfig;

    private void startTask(String description) {
        statusValueLabel.setText(description);
    }

    private void endTask() {
        statusValueLabel.setText("Done.");
        statusValueLabel.updateUI();
    }

    private void initClientList() {
        DataCollector.setQueryId("clients");
        DataCollector dc = new DataCollector(-1, appConfig);
        dc.executeQuery();
        List clients = dc.getClientsList();

        clientList.addItem(new Client(null, "*Select the client", ""));
        for (int i = 0; i < clients.size(); i++) {
            clientList.addItem(clients.get(i));
        }
    }


    public MainForm() {
        // App external resources.
        String configFile = "env/config.json";

        // Load configuration.
        String content = "", l;

        try {
            BufferedReader buff = new BufferedReader(new InputStreamReader(new FileInputStream(configFile), "UTF-8"));
            while ((l = buff.readLine()) != null) {
                content = content.concat(l);
            }
            buff.close();
        } catch (FileNotFoundException e) {
            System.out.println("File: 'env/config.json' not found,");
            System.exit(1);
        } catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported encoding!");
            System.exit(2);
        } catch (IOException e) {
            System.exit(3);
        }

        Gson gson = new Gson();
        appConfig = gson.fromJson(content, JsonObject.class);
        System.out.println("Application mode: " + appConfig.get("app_mode").getAsString());

        // Initialize GUI components.
        initClientList();

        generateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String invoiceId = invoiceIdField.getText();
                startTask("Generateing UPN QR code for: " + invoiceId);

                DataCollector dc = null;
                QrGenerator qrUpn = null;
                try {
                    DataCollector.setQueryId("invoice");
                    dc = new DataCollector(Integer.valueOf(invoiceId), appConfig);
                    qrUpn = new QrGenerator();

                    // Basic info.
                    qrUpn.setUpnDetail("ibanPlacnika", "");
                    qrUpn.setUpnDetail("polog", "");
                    qrUpn.setUpnDetail("dvig", "");

                    // Payer.
                    qrUpn.setUpnDetail("placnikReferenca", "");
                    qrUpn.setUpnDetail("placnikIme", dc.getPlacnikIme());
                    qrUpn.setUpnDetail("placnikUlica", dc.getPlacnikUlica());
                    qrUpn.setUpnDetail("placnikKraj", dc.getPlacnikKraj());

                    // Payment details.
                    // String validAmount = qrUpn.getAmountString(Double.valueOf("1.00"));
                    qrUpn.setUpnDetail("znesek", dc.getZnesek());
                    qrUpn.setUpnDetail("datumPlacila", "");
                    qrUpn.setUpnDetail("nujno", "");
                    qrUpn.setUpnDetail("kodaNamena", dc.getKodaNamena());
                    qrUpn.setUpnDetail("namenPlacila", dc.getNamenPlacila());
                    qrUpn.setUpnDetail("rokPlacila", dc.getRokPlacila());

                    // Receiver.
                    qrUpn.setUpnDetail("prejemnikIban", dc.getPrejemnikIban());
                    qrUpn.setUpnDetail("prejemnikReferenca", dc.getPrejemnikReferenca());
                    qrUpn.setUpnDetail("prejemnikIme", dc.getPrejemnikIme());
                    qrUpn.setUpnDetail("prejemnikUlica", dc.getPrejemnikUlica());
                    qrUpn.setUpnDetail("prejemnikKraj", dc.getPrejemnikKraj());

                    // Finalize it.
                    qrUpn.finalizeCode();

                    // Write QR code to database,
                    String qrCodeString = new String(qrUpn.getCode());
                    if (appConfig.get("app_mode").getAsString().equals("production")) {
                        dc.updateQrCode(qrCodeString);
                    }

                    // Generate report.
                    Report report = new Report(
                            appConfig.get("assets_path").getAsString(),
                            appConfig.get("report_path").getAsString(),
                            dc
                    );
                    report.setVarQrCode(qrCodeString);
                    report.generate();
                    // Open report in browser.
                    report.open();

                    // Cleanup.
                    dc.close();

                } catch (SQLException f) {
                    System.out.println("Read/Write to database failed.");
                    f.printStackTrace();
                    System.exit(4);
                } catch (IOException | WriterException g) {
                    System.out.println("Failed to generate QR code.");
                    g.printStackTrace();
                    System.exit(5);
                }
                endTask();


            }
        });
        signInvoiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String invoiceId = invoiceIdField.getText();
                startTask("Adding signature to invoice: " + invoiceId);

                SignatureGenerator signatureObj = new SignatureGenerator(appConfig.get("signature").getAsString());
                DataCollector.setQueryId("invoice");
                DataCollector dc = new DataCollector(Integer.valueOf(invoiceId), appConfig);

                if (appConfig.get("app_mode").getAsString().equals("production")) {
                    try {
                        dc.updateSignature(signatureObj.get());
                    } catch (IOException f) {
                        System.out.println("Signature file not found or there was error during the processing.");
                    } catch (SQLException f) {
                        System.out.println("Read/Write to database failed.");
                    }
                } else {
                    System.out.println("Signature can only be added if app is running in production mode, nothing has been done.");
                }

                endTask();
            }
        });
        downloadPDFButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String invoiceId = invoiceIdField.getText(),
                        downloadPath = localInvoiceFolderField.getText();

                // Make it cross platform and secure.
                downloadPath = downloadPath.replace("\\", "/");
                downloadPath = downloadPath.replaceAll("/$", "");
                File filePath = new File(downloadPath);
                if (!filePath.exists()) {
                    startTask("Invalid local location.");
                    return;
                }

                startTask("Downloading PDF for invoice: " + invoiceId);

                DataCollector.setQueryId("invoice");
                DataCollector dc = new DataCollector(Integer.valueOf(invoiceId), appConfig);
                String url = null;
                try {
                    url = dc.getOtherJavnaUrlPot();
                    if (url == null) {
                        startTask("Failed to download PDF.");
                        return;
                    }
                    String pdfPath = downloadPath + "/" + (new File(downloadPath)).getName() + ".pdf";
                    URL urlStream = new URL(url);
                    // Download from the server.
                    ReadableByteChannel readableByteChannel = Channels.newChannel(urlStream.openStream());
                    FileOutputStream fileOutputStream = new FileOutputStream(pdfPath);
                    FileChannel fileChannel = fileOutputStream.getChannel();
                    fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                } catch (SQLException f) {
                    System.out.println("Read/Write to database failed.");
                } catch (MalformedURLException f) {
                    System.out.println("Invalid url.");
                } catch (FileNotFoundException f) {
                    System.out.println("File not found.");
                } catch (IOException f) {
                    System.out.println("File read/write error.");
                }

                endTask();
            }
        });
        clientList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startTask("Getting client details.");
                Client client = (Client) clientList.getSelectedItem();

                // This object exists so we can show 'Select client.'
                if (client.getClientId() == null) {
                    return;
                }

                DataCollector.setQueryId("last-invoice");
                DataCollector dc = new DataCollector(-1, appConfig);
                dc.setClientId(client.getClientId());
                dc.updateQuery();
                dc.executeQuery();
                Invoice invoice = dc.getLastClientInvoice();
                dc.close();

                clientNameField.setText(client.getName());
                clientLastInvoiceOnField.setText(invoice.getInvoiceCreated().toString());
                clientLastInvoiceStatusField.setText(invoice.getInvoiceStatus());
                endTask();
            }
        });
        newReportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Client client = (Client) clientList.getSelectedItem();

                // Only for valid client.
                if (client.getClientId() == null) {
                    return;
                }

                NewReportForm dialog = new NewReportForm();
                dialog.pack();
                dialog.setVisible(true);
            }
        });
        generateEndYearReportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String currentYear = String.valueOf(Year.now().getValue());
                startTask("Generating report for: " + currentYear);
                DataCollector.setQueryId("end-report");
                DataCollector dc = new DataCollector(-1, appConfig);
                dc.setReportYear(currentYear);
                dc.updateQuery();
                dc.executeQuery();

                // Report meta.
                String reportPath = appConfig.get("report_path").getAsString() + "/" + currentYear + "-generirana-evidenca-izdanih-racunov.xlsx";

                // Generate file.
                XSSFWorkbook workbook = new XSSFWorkbook();
                Sheet sheet = workbook.createSheet("EIR");

                CreationHelper helper = workbook.getCreationHelper();

                // Cell types.
                Font boldText = workbook.createFont();
                boldText.setBold(true);
                CellStyle boldStyle = workbook.createCellStyle();
                boldStyle.setFont(boldText);

                CellStyle dateStyle = workbook.createCellStyle();
                dateStyle.setDataFormat(helper.createDataFormat().getFormat("dd/mm/yyy"));

                CellStyle moneyStyle = workbook.createCellStyle();
                moneyStyle.setDataFormat(helper.createDataFormat().getFormat("#.##0,0"));


                // Header
                Row headerRow = sheet.createRow(0);
                String[] headerStrings = {
                        "Status",
                        "Datum knjizbe",  // invoiceCreated
                        "St. listine",  // invoiceNumber
                        "Datum listine",
                        "Datum zacetka storitve",
                        "Datum opravljene storitve",
                        "Opis dogodka",
                        "Naziv stranke",
                        "Cisti prihodki od prodaje[EUR]"
                };
                for (int i = 0; i < headerStrings.length; i++) {
                    Cell cell = headerRow.createCell(i);
                    cell.setCellValue(headerStrings[i]);
                    cell.setCellStyle(boldStyle);
                }

                // Write invoice data to file.
                List<Invoice> invoiceList = dc.getReportInvoicesList();
                Row dataRow;
                Cell dataCell;
                // j - Row index nad it has to be one because we have header at 0.
                int j = 1, k;

                Double total = 0.0;

                for (Invoice invoice : invoiceList) {

                    dataRow = sheet.createRow(j);
                    k = 0;

                    dataCell = dataRow.createCell(k++);
                    dataCell.setCellValue(invoice.getInvoiceStatus());

                    dataCell = dataRow.createCell(k++);
                    dataCell.setCellStyle(dateStyle);
                    dataCell.setCellValue(invoice.getInvoicePosted());

                    dataCell = dataRow.createCell(k++);
                    dataCell.setCellValue(invoice.getInvoiceNumber());

                    dataCell = dataRow.createCell(k++);
                    dataCell.setCellStyle(dateStyle);
                    dataCell.setCellValue(invoice.getInvoiceCreated());


                    dataCell = dataRow.createCell(k++);
                    dataCell.setCellValue(invoice.getWorkFrom());

                    dataCell = dataRow.createCell(k++);
                    dataCell.setCellValue(invoice.getWorkTo());

                    dataCell = dataRow.createCell(k++);
                    dataCell.setCellValue(invoice.getSummary());

                    dataCell = dataRow.createCell(k++);
                    dataCell.setCellValue(invoice.getClientName());

                    dataCell = dataRow.createCell(k);
                    dataCell.setCellStyle(moneyStyle);
                    dataCell.setCellValue(invoice.getTotal());

                    total += invoice.getTotal();

                    j++;
                }

                // Stats
                j++;
                dataRow = sheet.createRow(j++);
                dataCell = dataRow.createCell(0);
                dataCell.setCellValue("Prihodki[EUR]");
                dataCell = dataRow.createCell(1);
                dataCell.setCellStyle(moneyStyle);
                dataCell.setCellValue(total);

                Double vatExpanses = total * 0.8;
                dataRow = sheet.createRow(j++);
                dataCell = dataRow.createCell(0);
                dataCell.setCellValue("Davcno priznani odhodki[EUR]");
                dataCell = dataRow.createCell(1);
                dataCell.setCellStyle(moneyStyle);
                dataCell.setCellValue(vatExpanses);

                Double vatBase = total - vatExpanses;
                dataRow = sheet.createRow(j++);
                dataCell = dataRow.createCell(0);
                dataCell.setCellValue("Davcna osnova[EUR]");
                dataCell = dataRow.createCell(1);
                dataCell.setCellStyle(moneyStyle);
                dataCell.setCellValue(vatBase);

                Double payVat = vatBase * 0.2;
                dataRow = sheet.createRow(j);
                dataCell = dataRow.createCell(0);
                dataCell.setCellValue("Davek[EUR]");
                dataCell = dataRow.createCell(1);
                dataCell.setCellStyle(moneyStyle);
                dataCell.setCellValue(payVat);

                // Adjust size of columns.
                for (int i = 0; i < headerStrings.length; i++) {
                    sheet.autoSizeColumn(i);
                }

                // Save stuff.
                try {
                    File file = new File(reportPath);

                    // Do nothing if file already exists.
                    if (file.exists()) {
                        System.out.println("Yearly report already exists, nothing done.");
                        startTask("Failed.");
                        return;
                    }

                    FileOutputStream fo = new FileOutputStream(reportPath);
                    workbook.write(fo);
                    fo.close();
                    workbook.close();
                    System.out.println("Yearly report is in {report_path}.");

                } catch (IOException f) {
                    System.out.println("Generating new report failed.");
                }
                endTask();

            }
        });
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("Quick Invoice");
        frame.setContentPane(new MainForm().quickInvoicePanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        quickInvoicePanel = new JPanel();
        quickInvoicePanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(11, 1, new Insets(0, 0, 0, 0), -1, -1));
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        quickInvoicePanel.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        reportPanel = new JPanel();
        reportPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(5, 3, new Insets(0, 0, 0, 0), -1, -1));
        quickInvoicePanel.add(reportPanel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        reportPanel.setBorder(BorderFactory.createTitledBorder("CLIENT REPORT"));
        clientLabel = new JLabel();
        clientLabel.setText("Client");
        reportPanel.add(clientLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        clientList = new JComboBox();
        reportPanel.add(clientList, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        clientNameLabel = new JLabel();
        clientNameLabel.setText("Name");
        reportPanel.add(clientNameLabel, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        clientNameField = new JTextField();
        reportPanel.add(clientNameField, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        clientLastInvoiceOnLabel = new JLabel();
        clientLastInvoiceOnLabel.setText("Last invoice on");
        reportPanel.add(clientLastInvoiceOnLabel, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        clientLastInvoiceOnField = new JTextField();
        reportPanel.add(clientLastInvoiceOnField, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        clientLastInvoiceStatusLabel = new JLabel();
        clientLastInvoiceStatusLabel.setText("Last invoice status");
        reportPanel.add(clientLastInvoiceStatusLabel, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        clientLastInvoiceStatusField = new JTextField();
        reportPanel.add(clientLastInvoiceStatusField, new com.intellij.uiDesigner.core.GridConstraints(3, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        localInvoiceFolderField = new JTextField();
        reportPanel.add(localInvoiceFolderField, new com.intellij.uiDesigner.core.GridConstraints(4, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        localInvoiceFolderLabel = new JLabel();
        localInvoiceFolderLabel.setText("Invoice location");
        reportPanel.add(localInvoiceFolderLabel, new com.intellij.uiDesigner.core.GridConstraints(4, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        upnCodePanel = new JPanel();
        upnCodePanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        quickInvoicePanel.add(upnCodePanel, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        upnCodePanel.setBorder(BorderFactory.createTitledBorder("UPN QR CODE"));
        upnCodeLabel = new JLabel();
        upnCodeLabel.setText("Invoice ID");
        upnCodePanel.add(upnCodeLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        invoiceIdField = new JTextField();
        upnCodePanel.add(invoiceIdField, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        newReportButton = new JButton();
        newReportButton.setText("New report");
        quickInvoicePanel.add(newReportButton, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        emailMessagePanel = new JPanel();
        emailMessagePanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        quickInvoicePanel.add(emailMessagePanel, new com.intellij.uiDesigner.core.GridConstraints(8, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        emailMessagePanel.setBorder(BorderFactory.createTitledBorder("EMAIL MESSAGE"));
        emailAddressLabel = new JLabel();
        emailAddressLabel.setText("Email address");
        emailMessagePanel.add(emailAddressLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        emailSubjectLabel = new JLabel();
        emailSubjectLabel.setText("Subject");
        emailMessagePanel.add(emailSubjectLabel, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        emailSubjectField = new JTextField();
        emailMessagePanel.add(emailSubjectField, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        emailValueLabel = new JLabel();
        emailValueLabel.setText("[ select the client ]");
        emailMessagePanel.add(emailValueLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer2 = new com.intellij.uiDesigner.core.Spacer();
        quickInvoicePanel.add(spacer2, new com.intellij.uiDesigner.core.GridConstraints(7, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        generateButton = new JButton();
        generateButton.setText("Generate");
        quickInvoicePanel.add(generateButton, new com.intellij.uiDesigner.core.GridConstraints(4, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        statusToolBar = new JToolBar();
        quickInvoicePanel.add(statusToolBar, new com.intellij.uiDesigner.core.GridConstraints(10, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(-1, 20), null, 0, false));
        statusLabel = new JLabel();
        statusLabel.setText("Status");
        statusToolBar.add(statusLabel);
        final com.intellij.uiDesigner.core.Spacer spacer3 = new com.intellij.uiDesigner.core.Spacer();
        statusToolBar.add(spacer3);
        statusValueLabel = new JLabel();
        statusValueLabel.setText("");
        statusToolBar.add(statusValueLabel);
        signInvoiceButton = new JButton();
        signInvoiceButton.setText("Sign Invoice");
        quickInvoicePanel.add(signInvoiceButton, new com.intellij.uiDesigner.core.GridConstraints(5, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        downloadPDFButton = new JButton();
        downloadPDFButton.setText("Download PDF");
        quickInvoicePanel.add(downloadPDFButton, new com.intellij.uiDesigner.core.GridConstraints(6, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        otherComponentsPanel = new JPanel();
        otherComponentsPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        quickInvoicePanel.add(otherComponentsPanel, new com.intellij.uiDesigner.core.GridConstraints(9, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        otherComponentsPanel.setBorder(BorderFactory.createTitledBorder("OTHER"));
        generateEndYearReportButton = new JButton();
        generateEndYearReportButton.setText("Generate current year report");
        otherComponentsPanel.add(generateEndYearReportButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return quickInvoicePanel;
    }

}

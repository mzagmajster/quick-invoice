package com.mzagmajster.invoice;

import java.io.*;
import java.sql.SQLException;
import java.nio.charset.StandardCharsets;
import java.awt.Desktop;


public class Report {

    private static final String REPORT_TEMPLATE_FILE = "template.html";

    private static final String EMAIL_TEMPLATE_FILE = "message-body.html";

    private static final String REPORT_FILE = "report.html";

    private String templatePath;  // Assets folder to be more exact (without trailing slash).

    private String reportPath;

    private String reportContent;

    private String messageContent;

    private DataCollector dataCollector;

    private String varQrCode;

    private String outFilePath;

    public Report(String templatePath, String reportPath, DataCollector dataCollector) {
        this.templatePath = templatePath;
        this.reportPath = reportPath;
        this.reportContent = null;
        this.messageContent = null;
        this.dataCollector = dataCollector;
        this.outFilePath = this.reportPath + "/" + REPORT_FILE;
    }

    public String readTemplate(String path) throws FileNotFoundException, IOException {
        File f = new File(path);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(f), "UTF8"));

        String content = "", l = "";
        while ((l = in.readLine()) != null) {
            content += l + "\n";
        }

        in.close();
        return content;
    }

    public int generate() {
        try {
            String p = templatePath + "/" + REPORT_TEMPLATE_FILE;
            reportContent = readTemplate(p);

            p = templatePath + "/" + EMAIL_TEMPLATE_FILE;
            messageContent = readTemplate(p);
        } catch (FileNotFoundException e) {
            return 1;
        } catch (IOException e) {
            return 2;
        }

        try {
            // Get email details.
            messageContent = messageContent.replace("{placnikPolnoIme}", dataCollector.getReportPlacnikPolnoIme());
            messageContent = messageContent.replace("{obdobjeOd}", dataCollector.getReportObdobjeOd());
            messageContent = messageContent.replace("{obdobjeDo}", dataCollector.getReportObdobjeDo());

            // Fill with data.
            reportContent = reportContent.replace("{stevilkaRacuna}", dataCollector.getReportStevilkaRacuna());
            reportContent = reportContent.replace("{qrCode}", varQrCode);

            // Subject line.
            String subjectLine = dataCollector.config.get("email_subject").toString().replace("{stevilkaRacuna}",
                    dataCollector.getReportStevilkaRacuna());
            // For some reason we get double quotes too so we have to remove first and last char,
            subjectLine = subjectLine.substring(1, (subjectLine.length() - 1));

            // Mail to params.
            String email = dataCollector.getReportStrankaEposta();
            String p = email + "?subject=" +
                    subjectLine +
                    "&body=" +
                    messageContent;
            reportContent = reportContent.replace("{sporociloParametri}", p);
            reportContent = reportContent.replace("{strankaEposta}", email);
            reportContent = reportContent.replace("{sporociloZadeva}", subjectLine);
        } catch (SQLException e) {
            e.printStackTrace();
            return 4;
        }

        // Save report.
        try {
            Writer fstream = new OutputStreamWriter(new FileOutputStream(outFilePath), StandardCharsets.UTF_8);
            fstream.write(reportContent);
            fstream.close();
        } catch (IOException e) {
            return 3;
        }

        return 0;
    }

    public void setVarQrCode(String data) {
        varQrCode = data;
    }

    public void open() throws  IOException {
        File rp = new File(outFilePath);
        System.out.println(outFilePath);
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            desktop.browse(rp.toURI());
        }

    }
}

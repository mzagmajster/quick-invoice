package com.mzagmajster.invoice;

import org.apache.commons.io.FileUtils;
import java.io.*;
import java.util.Base64;


public class SignatureGenerator {
    String path;

    public SignatureGenerator(String imagePath) {
        path = imagePath;
    }

    public String get() throws IOException {
        byte[] fileContent = FileUtils.readFileToByteArray(new File(path));
        return Base64.getEncoder().encodeToString(fileContent);
    }



}

package com.mzagmajster.invoice;

import java.io.*;
import java.sql.SQLException;

import com.google.zxing.WriterException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.HelpFormatter;

import com.mzagmajster.invoice.QrGenerator;
import com.mzagmajster.invoice.DataCollector;
import com.mzagmajster.invoice.Report;
import com.mzagmajster.invoice.SignatureGenerator;


public class Main
{
    public static void main( String[] args )
    {
    	// App external resources.
		String configFile = "env/config.json";
		
		// Load configuration.
		JsonObject appConfig;
		String content = "", l;

		try {
			BufferedReader buff  = new BufferedReader(new InputStreamReader(new FileInputStream(configFile), "UTF-8"));
			while ((l = buff.readLine()) != null) {
				content = content.concat(l);
			}
			buff.close();
		} catch (FileNotFoundException e) {
			System.out.println("File: 'env/config.json' not found,");
			System.exit(1);
		} catch (UnsupportedEncodingException e) {
			System.out.println("Unsupported encoding!");
			System.exit(2);
		} catch (IOException e) {
			System.exit(3);
		}
		
		Gson gson = new Gson();
		appConfig = gson.fromJson(content, JsonObject.class);
		System.out.println("Application mode: " + appConfig.get("app_mode").getAsString());

		Options appOptions = new Options();
		appOptions.addOption(new Option("i", "invoice-id", true, "Specify invoice ID. [REQUIRED]"));
		appOptions.addOption(new Option("c", "code", false, "Generate QR code for UPN."));
		appOptions.addOption(new Option("s", "sign", false, "Add signature to the invoice."));
		appOptions.addOption(new Option("h", "help", false, "Print help messages."));

		CommandLine appCmd = null;
		DataCollector dc = null;
		QrGenerator qrUpn = null;
		try {
			CommandLineParser appParser = new DefaultParser();
			appCmd = appParser.parse(appOptions, args);
		} catch (ParseException e) {
			System.out.println("Parsing failed, see --help for more details.");
			System.exit(1);
		}

		if (appCmd.hasOption("help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("app", appOptions);
			System.exit(0);
		}

		if (!appCmd.hasOption("invoice-id")) {
			System.out.println("Option: invoice-id is required.");
			System.exit(1);
		}

		if (appCmd.hasOption("code")) {
			try {
				dc = new DataCollector(Integer.valueOf(appCmd.getOptionValue("invoice-id")), appConfig);
				qrUpn = new QrGenerator();

				// Basic info.
				qrUpn.setUpnDetail("ibanPlacnika", "");
				qrUpn.setUpnDetail("polog", "");
				qrUpn.setUpnDetail("dvig", "");

				// Payer.
				qrUpn.setUpnDetail("placnikReferenca", "");
				qrUpn.setUpnDetail("placnikIme", dc.getPlacnikIme());
				qrUpn.setUpnDetail("placnikUlica", dc.getPlacnikUlica());
				qrUpn.setUpnDetail("placnikKraj", dc.getPlacnikKraj());

				// Payment details.
				// String validAmount = qrUpn.getAmountString(Double.valueOf("1.00"));
				qrUpn.setUpnDetail("znesek", dc.getZnesek());
				qrUpn.setUpnDetail("datumPlacila", "");
				qrUpn.setUpnDetail("nujno", "");
				qrUpn.setUpnDetail("kodaNamena", dc.getKodaNamena());
				qrUpn.setUpnDetail("namenPlacila", dc.getNamenPlacila());
				qrUpn.setUpnDetail("rokPlacila", dc.getRokPlacila());

				// Receiver.
				qrUpn.setUpnDetail("prejemnikIban", dc.getPrejemnikIban());
				qrUpn.setUpnDetail("prejemnikReferenca", dc.getPrejemnikReferenca());
				qrUpn.setUpnDetail("prejemnikIme", dc.getPrejemnikIme());
				qrUpn.setUpnDetail("prejemnikUlica", dc.getPrejemnikUlica());
				qrUpn.setUpnDetail("prejemnikKraj", dc.getPrejemnikKraj());

				// Finalize it.
				qrUpn.finalizeCode();

				// Write QR code to database,
				String qrCodeString = new String(qrUpn.getCode());
				if (appConfig.get("app_mode").getAsString().equals("production")) {
					dc.updateQrCode(qrCodeString);
				}

				// Generate report.
				Report report = new Report(
						appConfig.get("assets_path").getAsString(),
						appConfig.get("report_path").getAsString(),
						dc
				);
				report.setVarQrCode(qrCodeString);
				report.generate();
				// Open report in browser.
				report.open();

				// Cleanup.
				dc.close();

			} catch (SQLException e) {
				System.out.println("Read/Write to database failed.");
				e.printStackTrace();
				System.exit(4);
			} catch (IOException  | WriterException e) {
				System.out.println("Failed to generate QR code.");
				e.printStackTrace();
				System.exit(5);
			}
		}

		if (appCmd.hasOption("sign")) {
			SignatureGenerator signatureObj = new SignatureGenerator(appConfig.get("signature").getAsString());
			dc = new DataCollector(Integer.valueOf(appCmd.getOptionValue("invoice-id")), appConfig);


			if (appConfig.get("app_mode").getAsString().equals("production")) {
				try {
					dc.updateSignature(signatureObj.get());
				} catch (IOException e) {
					System.out.println("Signature file not found or there was error during the processing.");
				}  catch (SQLException e) {
					System.out.println("Read/Write to database failed.");
				}
			}
			else {
				System.out.println("Signature can only be added if app is running in production mode, nothing has been done.");
			}
		}
    }
}

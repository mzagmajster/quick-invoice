package com.mzagmajster.invoice;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Hashtable;

import com.google.zxing.EncodeHintType;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;


public class QrGenerator {
	private String upnText;
	final int QrSize = 150;
	
	public QrGenerator() {
		
		/**
		 * Fields {kontrolnaVsota} and {rezerva} are added automatically when we finalize the code.
		 */
		this.upnText = "UPNQR\n" + 
				"{ibanPlacnika}\n" + 
				"{polog}\n" + 
				"{dvig}\n" + 
				"{placnikReferenca}\n" + 
				"{placnikIme}\n" + 
				"{placnikUlica}\n" + 
				"{placnikKraj}\n" + 
				"{znesek}\n" + 
				"{datumPlacila}\n" + 
				"{nujno}\n" + 
				"{kodaNamena}\n" + 
				"{namenPlacila}\n" + 
				"{rokPlacila}\n" + 
				"{prejemnikIban}\n" + 
				"{prejemnikReferenca}\n" + 
				"{prejemnikIme}\n" + 
				"{prejemnikUlica}\n" + 
				"{prejemnikKraj}\n";
	}
	
	public byte[] getCode() throws WriterException, IOException {
		Hashtable hints = new Hashtable();
		hints.put(EncodeHintType.CHARACTER_SET, "ISO-8859-2");
				
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(upnText, BarcodeFormat.QR_CODE, QrSize, QrSize, hints);

        ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream);
        byte[] pngData = pngOutputStream.toByteArray();
        return Base64.getEncoder().encode(pngData);
	}
	
	public void setUpnDetail(String placeholder, String value) {
		String templateVar = "{" + placeholder + "}";
		upnText = upnText.replace(templateVar, value);
	}
	
	public void finalizeCode() {
		// ISO-8859-2 IBM00858 Cp852
		
		Integer size = Integer.valueOf(upnText.length());
		String sizeString = size.toString();
		while (sizeString.length() < 3) {
			sizeString = "0" + sizeString;
		}
		
		upnText += sizeString + "\n";		
	}
	
	public String getUpnText() {
		return upnText;
	}
}

package com.mzagmajster.invoice;

import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.nio.file.Files;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.google.gson.JsonObject;

import com.mzagmajster.invoice.Client;
import com.mzagmajster.invoice.InvoiceValidationException;
import com.sun.tools.doclets.standard.Standard;


public class DataCollector {

    private String sql = null;

	private Connection conn = null;
	
	private Statement stmt = null;
	
	private ResultSet rs = null;
	
	private Integer invoiceId;

	private Integer clientId;

	private String reportYear;
		
	final private int MAX_LENGTH_TYPICAL = 33;

	private static String[] ALLOWED_QUERIES = {"clients", "end-report", "invoice", "last-invoice"};

	private static String queryId = "invoice";  // This does not have desired affect.

	public JsonObject config = null;

	public static void setQueryId(String queryId) {
        DataCollector.queryId = queryId;
    }
	
	public DataCollector(Integer invoiceId, JsonObject config) {
		this.config = config;
		this.invoiceId = invoiceId;

		try {
            // Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(
					config.get("db_url").getAsString(),
					config.get("db_user").getAsString(),
					config.get("db_password").getAsString()
				);
			stmt = conn.createStatement();

			if (Arrays.asList(ALLOWED_QUERIES).contains(queryId)) {
			    try {
                    String path = config.get("assets_path").getAsString() + "/" +  queryId + ".sql";
                    sql = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
                } catch (IOException e) {
			        this.close();
			        return;
                }
			}

            // Specific invoice query - so we do not have change calls elsewhere.
            if (queryId.equals("invoice")) {
                sql = sql.replace("?", invoiceId.toString());
                rs = stmt.executeQuery(sql);
                rs.next();
            }
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void close() {
		try {
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void setClientId(Integer clientId) {
	    this.clientId = clientId;
    }

    public void setReportYear(String reportYear) {
		this.reportYear = reportYear;
	}

    public void updateQuery() {
	    if (queryId.equals("last-invoice")) {
            sql = sql.replace("?", clientId.toString());
        }
	    else if (queryId.equals("end-report")) {
	    	sql = sql.replace("?", reportYear);
		}
    }

    public void executeQuery() {
	    try {
	    	if (config.get("app_mode").getAsString().equals("development")) {
	    		System.out.println(sql);
			}

            rs = stmt.executeQuery(sql);
        } catch (SQLException e) {
	        return;
        }
    }

	// UPN QR code stuff
	// Basic info.
	public String getIbanPlacnika() {
		return "";
	}
	
	public String getPolog() {
		return "";
	}
	
	public String getDvig() {
		return "";
	}
	
	// Payer.
	public String getPlacnikReferenca() {
		return "";
	}
	
	public String getPlacnikIme() throws SQLException {
		// Database value
		String value = rs.getString("placnikIme");
		
		// Formatting.
		// Note: Its always better pre-prepare the system so no client exceeds max length constraint.
		value.trim();
		int removeCharsIndex = MAX_LENGTH_TYPICAL - value.length();
		if (removeCharsIndex < 0) {
			value = value.substring(0, removeCharsIndex);
		}
		
		return value;
	}
	
	public String getPlacnikUlica() throws SQLException {
		// Database value
		String value = rs.getString("placnikUlica");
		
		// Formatting.
		value.trim();
		int removeCharsIndex = MAX_LENGTH_TYPICAL - value.length();
		if (removeCharsIndex < 0) {
			value = value.substring(0, removeCharsIndex);
		}
		
		return value;
	}
	
	public String getPlacnikKraj() throws SQLException {
		// Database value
		String value = rs.getString("placnikKraj");
		
		// Formatting.
		value.trim();
		int removeCharsIndex = MAX_LENGTH_TYPICAL - value.length();
		if (removeCharsIndex < 0) {
			value = value.substring(0, removeCharsIndex);
		}
		
		return value;
	}
	
	// Payment details. 
	public String getZnesek() throws InvoiceValidationException, SQLException {
		// Get value from database. 
		Double amount = rs.getDouble("znesek");
		
		// Format value for UPNQR standard.
		Double newAmount = amount * 100;
		
		String amountString = String.valueOf(newAmount.intValue());
		
		while (amountString.length() < 11) {
			amountString = "0" + amountString; 
		}
		
		if (amountString.length() > 11) {
			throw new InvoiceValidationException("Final amount string must not be longer then 11 chars.");
		}
		
		return amountString;
	}
	
	public String getDatumPlacila() {
		return "";
	}
	
	public String getNujno() {
		return "";
	}
	
	public String getKodaNamena() {
		return this.config.get("upn_purpose_code").getAsString();
	}
	
	public String getNamenPlacila() throws SQLException {
		return (config.get("upn_purpose_prefix").getAsString() + rs.getString("stevilkaRacuna"));
	}
	
	public String getRokPlacila() throws SQLException {
		return rs.getString("rokPlacila");
	}
	
	// Receiver.
	public String getPrejemnikIban() throws SQLException {
		return rs.getString("prejemnikIban");
	}
	
	public String getPrejemnikReferenca() {
		return config.get("upn_receiver_reference").getAsString();
	}
	
	public String getPrejemnikIme() throws SQLException {
		return rs.getString("prejemnikIme");
	}
	
	public String getPrejemnikUlica() throws SQLException {
		return rs.getString("prejemnikUlica");
	}
	
	public String getPrejemnikKraj() throws SQLException {
		return rs.getString("prejemnikKraj");
	}
	
	public int updateQrCode(String encodedUpnText) throws SQLException {
		String sql = "UPDATE invoices_custom " +
				"SET column_3 = '" + encodedUpnText  + "' " +
				"WHERE invoice_id = " + invoiceId.intValue() + "; ";


		Statement stmt_update = conn.createStatement();
		int rows_count = stmt_update.executeUpdate(sql);

		stmt_update.close();

		return rows_count;

	}

	// end of UPN QR code stuff

	// Report stuff.
	public String getReportStevilkaRacuna() throws SQLException {
		return rs.getString("stevilkaRacuna");
	}

	public String getReportObdobjeOd() throws  SQLException {
		String v = rs.getString("obdobjeOd");
		if (rs.wasNull()) {
			v = "";
		}
		return v;
	}

	public String getReportObdobjeDo() throws SQLException {
		String v = rs.getString("obdobjeDo");
		if (rs.wasNull()) {
			v = "";
		}

		return v;
	}

	public String getReportPlacnikPolnoIme() throws SQLException {
		// System.out.println(rs.getInt("IdRacuna"));
		return rs.getString("placnikPolnoIme");
	}

	public String getReportStrankaEposta() throws SQLException {
		String v = rs.getString("strankaEposta");
		if (rs.wasNull()) {
			v = "n-a";
		}

		return v;
	}

	// end of Report stuff

	// Other stuff
	public String getOtherJavnaUrlPot() throws SQLException {
		String v = rs.getString("javnaUrlPot");
		if (rs.wasNull()) {
			v = null;
		}
		else {
			v = config.get("public_access").getAsString() + "/" + v + "/pdf";
		}

		return v;
	}

	public int updateSignature(String encodedSignature) throws SQLException {
		String sql = "UPDATE invoices_custom " +
				"SET column_4 = '" + encodedSignature  + "' " +
				"WHERE invoice_id = " + invoiceId.intValue() + "; ";

		Statement stmt_update = conn.createStatement();
		int rows_count = stmt_update.executeUpdate(sql);
		stmt_update.close();

		return rows_count;
	}

	public List getClientsList() {
        List<Client> clients = new ArrayList<>();
        try {
            while(rs.next()) {
                Client c = new Client(
                        rs.getInt("clientId"),
                        rs.getString("alias"),
                        rs.getString("name")
                );
                clients.add(c);
            }
        } catch (SQLException e) {
            return clients;
        }

        return clients;

	}

	public List<Invoice> getReportInvoicesList() {
		List<Invoice> invoices = new ArrayList<>();
		try {
			Invoice i;
			while (rs.next()) {
				i = new Invoice();
				i.setInvoicePosted(rs.getDate("invoicePosted"));
				i.setInvoiceNumber(rs.getString("invoiceNumber"));
				i.setInvoiceCreated(rs.getDate("invoiceCreated"));
				i.setWorkTo(rs.getString("workTo"));

				String summary = rs.getString("summary");
				if (rs.wasNull()) {
					summary = "[auto generated]";
				}
				i.setSummary(summary);

				i.setClientName(rs.getString("clientName"));
				i.setTotal(rs.getDouble("total"));
				i.setInvoiceId(rs.getInt("invoiceId"));
				i.setInvoiceStatus(rs.getString("invoiceStatus"));
				i.setWorkFrom(rs.getString("workFrom"));

				invoices.add(i);

			}
		} catch (SQLException e) {
			return invoices;
		}

		return invoices;
	}

	public Invoice getLastClientInvoice () {
	    Invoice i = null;
	    try {
	        if (!rs.next() || rs.wasNull()) {
	            return i;
            }

            i = new Invoice();
	        i.setInvoiceCreated(rs.getDate("invoiceCreated"));
	        i.setInvoiceStatus(rs.getString("invoiceStatus"));
        } catch (SQLException e) {
	        return i;
        }

	    return i;

    }

	// end of Other stuff



}

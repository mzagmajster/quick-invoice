package com.mzagmajster.invoice;

import java.sql.Date;

public class Invoice {
    private Integer invoiceId;
    private String invoiceStatus;

    private String workFrom;
    private String workTo;
    private Date invoicePosted; // Knjizba
    private Date invoiceCreated;

    private String invoiceNumber;
    private String summary;
    private String clientName;
    private Double total;

    public Integer setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
        return this.invoiceId;
    }

    public Integer getInvoiceId() {
        return this.invoiceId;
    }

    public String setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
        return this.invoiceStatus;
    }

    public String getInvoiceStatus() {
        return this.invoiceStatus;
    }

    public String setWorkFrom(String workFrom) {
        this.workFrom = workFrom;
        return this.workFrom;
    }

    public String getWorkFrom() {
        return this.workFrom;
    }

    public String setWorkTo(String workTo) {
        this.workTo = workTo;
        return  this.workTo;
    }

    public String getWorkTo() {
        return this.workTo;
    }

    public Date setInvoicePosted(Date invoicePosted) {
        this.invoicePosted = invoicePosted;
        return this.invoicePosted;
    }

    public Date getInvoicePosted() {
        return this.invoicePosted;
    }

    public Date setInvoiceCreated(Date invoiceCreated) {
        this.invoiceCreated = invoiceCreated;
        return this.invoiceCreated;
    }

    public Date getInvoiceCreated() {
        return this.invoiceCreated;
    }

    public String setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
        return this.invoiceNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String setSummary(String summary) {
        this.summary = summary;
        return this.summary;
    }

    public String getSummary() {
        return summary;
    }

    public String setClientName(String clientName) {
        this.clientName = clientName;
        return this.clientName;
    }

    public String getClientName() {
        return clientName;
    }

    public Double setTotal(Double total) {
        this.total = total;
        return this.total;
    }

    public Double getTotal() {
        return total;
    }
}

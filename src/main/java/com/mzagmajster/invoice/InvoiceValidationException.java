package com.mzagmajster.invoice;

public class InvoiceValidationException extends RuntimeException {
	public InvoiceValidationException(String errorMessage) {
		super(errorMessage);
	}
}
